Pod::Spec.new do |s|
  s.name              = "AugmentedExhibition"
  s.version           = "0.2.0"
  s.summary           = "This library serves for augmenting real world exhibition with AR."
  s.license           = "EULA"
  s.author            = { 'SYNETECH s.r.o.' => 'info@synetech.cz' }
	s.source            = { :git => "https://bitbucket.org/synetech/augmented-exhibition.git", :tag => s.version.to_s }
  s.social_media_url  = 'synetech.cz'
  s.homepage          = 'synetech.cz'
  s.platform          = :ios, "11.0"
  s.source_files      = 'Sources/**/*.swift'
  s.requires_arc      = true
  s.swift_versions    = '5.0'

  s.dependency 'SteviaLayout'
  s.dependency 'Swinject'
end
