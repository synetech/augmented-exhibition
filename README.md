# Augmented Exhibition

This library serves for augmenting real world exhibition with AR. It detects real objects and place virtual content around them.
Available as package or cocoapod.

## Usage

You only need to provide the reference objects and array of `DetectionObject`, which consists of `Entity`, which should be placed on the detected anchor and identifier - name of the reference object.

### Example

Definition of detection objects:
```swift
enum AppParade33DetectionObject: String, CaseIterable {
    case camera
}

extension AppParade33DetectionObject: DetectionObject {

    var identifier: String {
        return self.rawValue
    }

    var entity: HasAnchoring? {
        switch self {
            case .camera:
                return try? Camera.loadPanels()
        }
    }
}
```

And the implementation itself:
```swift
guard let referenceObjects = ARReferenceObject.referenceObjects(inGroupNamed: "AppParade33Exhibition",
                                                                bundle: nil) else { return }
let config = AEConfiguration(referenceObjects: referenceObjects,
                             detectionObjects: AppParade33DetectionObject.allCases)
let augmentedExhibition: AugmentedExhibition = AugmentedExhibitionImpl(config: config)
augmentedExhibition.startAugmentedExhibition(in: navigationController)
```

### Events

The library can emmit two events, which needs to be handled in the implementing app. Both of them indicates that the AR session can't be started.
```swift
/// User needs to be informed, that he has denied camera access and should have be provided with possibility to redirect to setting for the app.
case noCameraAccess
/// AR screen has failed from various reason (AR session fail, missing reference objects etc.). User should be informed and provided with option to retry.
case fail
```

### Customization

The configuration struct can also contain some customizations. You can provide your own *tranlations* (the library has only basic english translations) - all of them is listed within the `AETranslations` struct, which can be passed to the configuration.\n
Also you can provide *font* names (in the configuration) to match the implementing app.
