//
//  CameraEntityFactory.swift
//  AugmentedExhibitionDemoProject
//
//  Created by Lukáš Růžička on 07/11/2019.
//  Copyright © 2019 Synetech. All rights reserved.
//

import RealityKit

class CameraEntityFactory {

    // MARK: - Create
    static func createCameraEntity() -> HasAnchoring? {
        let camera = try? Camera.loadPanels()
        Interactions.setupPanelsInteractions(camera)
        return camera
    }

    class Interactions {

        private static var _1State: PanelState = .facingCamera
        private static var _2State: PanelState = .facingCamera
        private static var _3State: PanelState = .facingCamera
        private static var _4State: PanelState = .facingCamera

        fileprivate static func setupPanelsInteractions(_ camera: Camera.Panels?) {
            setupSceneStart(camera)
            setupFirstPanel(camera)
            setupSecondPanel(camera)
            setupThirdPanel(camera)
            setupFourthPanel(camera)
        }

        private static func setupSceneStart(_ camera: Camera.Panels?) {
            camera?.actions.sceneStart.onAction = { _ in
                camera?.notifications._1FaceCamera.post()
                camera?.notifications._2FaceCamera.post()
                camera?.notifications._3FaceCamera.post()
                camera?.notifications._4FaceCamera.post()
            }
        }

        private static func setupFirstPanel(_ camera: Camera.Panels?) {
            camera?.actions._1FacingCameraEnded.onAction = { _ in
                if _1State == .shouldAnimate {
                    camera?.notifications._1AnimatePanel.post()
                    return
                }
                camera?.notifications._1FaceCamera.post()
            }
            camera?.actions._1Tapped.onAction = { _ in
                guard _1State == .facingCamera else { return }
                _1State = .shouldAnimate
            }
            camera?.actions._1PanelAnimationEnded.onAction = { _ in
                _1State = .facingCamera
                camera?.notifications._1FaceCamera.post()
            }
        }

        private static func setupSecondPanel(_ camera: Camera.Panels?) {
            camera?.actions._2FacingCameraEnded.onAction = { _ in
                if _2State == .shouldAnimate {
                    camera?.notifications._2AnimatePanel.post()
                    return
                }
                camera?.notifications._2FaceCamera.post()
            }
            camera?.actions._2Tapped.onAction = { _ in
                guard _2State == .facingCamera else { return }
                _2State = .shouldAnimate
            }
            camera?.actions._2PanelAnimationEnded.onAction = { _ in
                _2State = .facingCamera
                camera?.notifications._2FaceCamera.post()
            }
        }

        private static func setupThirdPanel(_ camera: Camera.Panels?) {
            camera?.actions._3FacingCameraEnded.onAction = { _ in
                if _3State == .shouldAnimate {
                    camera?.notifications._3AnimatePanel.post()
                    return
                }
                camera?.notifications._3FaceCamera.post()
            }
            camera?.actions._3Tapped.onAction = { _ in
                guard _3State == .facingCamera else { return }
                _3State = .shouldAnimate
            }
            camera?.actions._3PanelAnimationEnded.onAction = { _ in
                _3State = .facingCamera
                camera?.notifications._3FaceCamera.post()
            }
        }

        private static func setupFourthPanel(_ camera: Camera.Panels?) {
            camera?.actions._4FacingCameraEnded.onAction = { _ in
                if _4State == .shouldAnimate {
                    camera?.notifications._4AnimatePanel.post()
                    return
                }
                camera?.notifications._4FaceCamera.post()
            }
            camera?.actions._4moreFacingCameraEnded.onAction = { _ in
                if _4State == .shouldAnimate {
                    camera?.notifications._4moreAnimatePanel.post()
                    return
                }
                camera?.notifications._4moreFaceCamera.post()
            }
            let fourthPanelTapped: (RealityKit.Entity?) -> Swift.Void = { _ in
                guard _4State == .facingCamera else { return }
                _4State = .shouldAnimate
            }
            camera?.actions._4Tapped.onAction = fourthPanelTapped
            camera?.actions._4moreTapped.onAction = fourthPanelTapped
            camera?.actions._4PanelAnimationEnded.onAction = { _ in
                _4State = .facingCamera
                camera?.notifications._4moreFaceCamera.post()
            }
            camera?.actions._4morePanelAnimationEnded.onAction = { _ in
                _4State = .facingCamera
                camera?.notifications._4FaceCamera.post()
            }
        }

        enum PanelState {
            case facingCamera
            case shouldAnimate
        }
    }
}
