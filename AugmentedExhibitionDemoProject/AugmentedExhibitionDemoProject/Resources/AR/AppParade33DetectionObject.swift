//
//  AppParade33DetectionObject.swift
//  AugmentedExhibitionDemoProject
//
//  Created by Lukáš Růžička on 02/10/2019.
//  Copyright © 2019 Synetech. All rights reserved.
//

import Exhibition
import RealityKit

enum AppParade33DetectionObject: String, CaseIterable {

    case camera
}

extension AppParade33DetectionObject: DetectionObject {

    var identifier: String {
        return self.rawValue
    }

    var entity: HasAnchoring? {
        switch self {
        case .camera:
            return CameraEntityFactory.createCameraEntity()
        }
    }
}
