//
//  LibRouter.swift
//  AugmentedExhibition
//
//  Created by Lukáš Růžička on 20/09/2019.
//  Copyright © 2019 Synetech. All rights reserved.
//

#if canImport(UIKit) && canImport(ARKit) && canImport(Combine)
import Combine
import Swinject
import UIKit

@available (iOS 13.0, *)
protocol LibRouterInit {

    func useNavigation(_ navigationController: UINavigationController)
    func presentInitialScreen()
    
    var eventSubject: PassthroughSubject<AEEvent, Error> { get }
}

protocol LibRouter {

    // MARK: - Internal navigation
    func onCancel()
    func showARScreen()
    func emmitEvent(_ event: AEEvent)
}

// swiftlint:disable force_unwrapping
@available (iOS 13.0, *)
class LibRouterImpl {

    // MARK: - Properties
    private var navigationController: UINavigationController?

    let eventSubject: PassthroughSubject<AEEvent, Error> = PassthroughSubject()

    // swiftlint:disable implicitly_unwrapped_optional
    private var container: Container!

    // MARK: - Init
    init(config: AEConfiguration) {
        container = DataSourceDependencies.build(parent: nil,
                                                 depth: .presentation,
                                                 config: config,
                                                 router: self)
    }
}

// swiftlint:disable force_unwrapping
@available (iOS 13.0, *)
extension LibRouterImpl: LibRouterInit {

    func useNavigation(_ navigationController: UINavigationController) {
        self.navigationController = navigationController
        navigationController.isNavigationBarHidden = true
    }

    func presentInitialScreen() {
        showARScreen()
    }

    func  getEventPublisher() -> PassthroughSubject<AEEvent, Error> {
        return eventSubject
    }
}

// swiftlint:disable force_unwrapping
@available (iOS 13.0, *)
extension LibRouterImpl: LibRouter {

    // MARK: - Internal navigation
    func onCancel() {
        if navigationController?.visibleViewController != navigationController?.topViewController {
            navigationController?.visibleViewController?.dismiss(animated: true, completion: nil)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }

    func showARScreen() {
        if let viewController = container.resolve(ARScreenViewController.self) {
            navigationController?.pushViewController(viewController, animated: true)
        }
    }

    func emmitEvent(_ event: AEEvent) {
        eventSubject.send(event)
    }
}
#endif
