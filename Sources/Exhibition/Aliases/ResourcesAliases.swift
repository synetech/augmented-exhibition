//
//  ResourcesAliases.swift
//  
//
//  Created by Lukáš Růžička on 05/11/2019.
//

typealias FontNames = (regular: String, bold: String?)
