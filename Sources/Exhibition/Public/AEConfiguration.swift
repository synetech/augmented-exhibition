//
//  AEConfiguration.swift
//
//
//  Created by Lukáš Růžička on 06/11/2019.
//

#if canImport(ARKit) && canImport(Combine)
import ARKit
import Combine
import RealityKit

@available (iOS 13.0, *)
public struct AEConfiguration {

    // MARK: - AR resources
    let referenceObjects: Set<ARReferenceObject>
    /// Define your own enum which conforms to protocol `DetectionObject` and pass `.allCases`
    let detectionObjects: [DetectionObject]

    // MARK: - Common resources
    let translations: AETranslations?
    let fontNames: FontNames?

    // MARK: - Init
    /// Configuration for the library.
    /// - Parameters:
    ///   - referenceObjects: Scanned real reference objects, which should be detected.
    ///   - detectionObjects: Define your own enum which conforms to protocol `DetectionObject` and pass `.allCases`.
    ///   - translations: All translations used in the library. If you don't provide them, basic english translations will be used.
    ///   - fontNames: Name of the regular font. Optionally you can add bold font also (if not provided, regular font is used everywhere).
    public init(referenceObjects: Set<ARReferenceObject>,
                detectionObjects: [DetectionObject],
                translations: AETranslations? = nil,
                fontNames: (regular: String, bold: String?)? = nil) {
        self.referenceObjects = referenceObjects
        self.detectionObjects = detectionObjects
        self.translations = translations
        self.fontNames = fontNames
    }
}

// MARK: - Detection object protocol
@available (iOS 13.0, *)
public protocol DetectionObject {

    /// Identifier should equal name of the reference object
    var identifier: String { get }
    /// The entity which should overlay the view. Should be loaded asynchronously from the `.rcproject` file
    var entity: Future<HasAnchoring, Error> { get }
}
#endif

// MARK: - Translations
public struct AETranslations {

    let initialInstructions: String
    let recommendationForExcessiveMotion: String
    let recommendationForInsufficientFeatures: String

    // MARK: - Init
    /// Translations of string used in the library
    /// - Parameters:
    ///   - initialInstructions: First message which is presented to user when he enter the AR session
    ///   - recommendationForExcessiveMotion: Info message for user, used when he moves with the device too fast
    ///   - recommendationForInsufficientFeatures: Info message for user, used when the environment around the device is too dark
    public init(initialInstructions: String,
                recommendationForExcessiveMotion: String,
                recommendationForInsufficientFeatures: String) {
        self.initialInstructions = initialInstructions
        self.recommendationForExcessiveMotion = recommendationForExcessiveMotion
        self.recommendationForInsufficientFeatures = recommendationForInsufficientFeatures
    }
}
