//
//  AugmentedExhibition.swift
//  AugmentedExhibition
//
//  Created by Lukáš Růžička on 20/09/2019.
//  Copyright © 2019 Synetech. All rights reserved.
//

#if canImport(UIKit) && canImport(ARKit) && canImport(Combine)
import Combine
import UIKit

@available (iOS 13.0, *)
public protocol AugmentedExhibition {

    func startAugmentedExhibition(in navigationController: UINavigationController) -> PassthroughSubject<AEEvent, Error>
}

@available (iOS 13.0, *)
public class AugmentedExhibitionImpl {

    // MARK: - Properties
    private let libRouter: LibRouterInit

    static let bundle = Bundle(for: AugmentedExhibitionImpl.self)

    // MARK: - Init
    public init(config: AEConfiguration) {
        libRouter = LibRouterImpl(config: config)
    }
}

// MARK: - Interaction
@available (iOS 13.0, *)
extension AugmentedExhibitionImpl: AugmentedExhibition {

    public func startAugmentedExhibition(in navigationController: UINavigationController) -> PassthroughSubject<AEEvent, Error> {
        libRouter.useNavigation(navigationController)
        libRouter.presentInitialScreen()

        return libRouter.eventSubject
    }
}
#endif
