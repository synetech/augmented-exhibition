//
//  RecommendationView.swift
//  AugmentedExhibition
//
//  Created by Lukáš Růžička on 26/09/2019.
//  Copyright © 2019 Synetech. All rights reserved.
//

#if canImport(UIKit)
import Stevia
import UIKit

class RecommendationView: UIView, LibFonts {

    // MARK: - Views
    private lazy var vRecommendationPanel: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        view.layer.cornerRadius = 8
        return view
    } ()

    private lazy var lblRecommendation: UILabel = {
        let view = UILabel()
        view.textAlignment = .center
        if let font = getFont(for: .regular, ofSize: 13) {
            view.font = font
        }
        view.textColor = .white
        view.numberOfLines = 2
        view.adjustsFontSizeToFitWidth = true
        return view
    } ()

    // MARK: - Constants
    private let edgeMargin: CGFloat = 8

    // MARK: - Variables
    var isShown: Bool = false

    private var messageTimer: Timer?

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)

        setupViews()
        makeLayout()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Layout
    private func setupViews() {
        backgroundColor = .clear
        alpha = 0
    }

    private func makeLayout() {
        sv(vRecommendationPanel.sv(lblRecommendation))
        vRecommendationPanel.centerHorizontally()
        vRecommendationPanel.Width <= Width
        vRecommendationPanel.layout(edgeMargin,
                                    |-edgeMargin - lblRecommendation - edgeMargin-|,
                                    edgeMargin)
    }

    func changeForLightBackground() {
        lblRecommendation.textColor = .black
        vRecommendationPanel.backgroundColor = UIColor.lightGray.withAlphaComponent(0.6)
    }

    // MARK: - Interactions
    func showMessage(_ message: String, showTime: TimeInterval = 3) {
        DispatchQueue.main.async { [weak self] in
            self?.messageTimer?.invalidate()
            self?.layer.removeAllAnimations()

            self?.showView(with: message, showTime: showTime)
        }
    }

    private func showView(with message: String, showTime: TimeInterval) {
        changeStatusAppearence(hide: true) { [weak self] in
            self?.lblRecommendation.text(message)
            self?.changeStatusAppearence(hide: false, completion: nil)
            self?.messageTimer = Timer.scheduledTimer(withTimeInterval: showTime,
                                                      repeats: false,
                                                      block: { [weak self] _ in
                                                        self?.changeStatusAppearence(hide: true, completion: nil)
            })
        }
    }

    func cancelMessages() {
        DispatchQueue.main.async { [weak self] in
            guard self?.alpha ?? 0.0 != 0.0 else { return }
            self?.messageTimer?.invalidate()
            self?.changeStatusAppearence(hide: true, completion: nil)
        }
    }

    private func changeStatusAppearence(hide: Bool, completion: (() -> Void)?) {
        isShown = !hide
        DispatchQueue.main.async { [weak self] in
            UIView.animate(withDuration: hide ? 0.1 : 0.2,
                           animations: {
                            self?.alpha = hide ? 0.0 : 1.0
            }, completion: { _ in
                completion?()
            })
        }
    }
}

#endif
