//
//  ARScreenViewModel.swift
//  AugmentedExhibition
//
//  Created by Lukáš Růžička on 26/09/2019.
//  Copyright © 2019 Synetech. All rights reserved.
//

#if canImport(ARKit) && canImport(Combine)
import ARKit
import Combine
import RealityKit

@available (iOS 13.0, *)
protocol ARScreenViewModel {

    // MARK: - Reference objects
    func getReferenceObjects() -> Set<ARReferenceObject>?

    // MARK: - Entities
    func preloadEntities()
    func getEntity(for name: String) -> CurrentValueSubject<HasAnchoring?, Error>?

    func disableAllEntities()
    func enableEntity(name: String)
    func isEntityEnabled(name: String) -> Bool

    func setEntityIsInScene(_ entity: HasAnchoring)
    func getAllEntitiesInScene() -> [HasAnchoring]
    func removeAllEntitiesFromScene()

    func clearEntitiesCache()
}
#endif
