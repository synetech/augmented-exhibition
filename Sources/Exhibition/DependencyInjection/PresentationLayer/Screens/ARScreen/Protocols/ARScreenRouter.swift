//
//  ARScreenRouter.swift
//  AugmentedExhibition
//
//  Created by Lukáš Růžička on 26/09/2019.
//  Copyright © 2019 Synetech. All rights reserved.
//

import Foundation

protocol ARScreenRouter {

    func onBackTapped()
    func onMissingCameraPermission()
    func onFail()
}
