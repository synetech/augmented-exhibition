//
//  EntityCacheModel.swift
//
//
//  Created by Lukáš Růžička on 07/11/2019.
//

#if canImport(ARKit) && canImport(Combine)
import Combine
import RealityKit

@available (iOS 13.0, *)
class EntityCacheModel {

    // MARK: - Properties
    let identifier: String
    let entity: CurrentValueSubject<HasAnchoring?, Error> = CurrentValueSubject(nil)
    var inScene: Bool = false

    // MARK: - Init
    init(identifier: String) {
        self.identifier = identifier
    }
}
#endif
