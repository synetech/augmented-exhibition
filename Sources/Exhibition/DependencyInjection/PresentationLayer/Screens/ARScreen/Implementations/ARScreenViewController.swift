//
//  ARScreenViewController.swift
//  AugmentedExhibition
//
//  Created by Lukáš Růžička on 26/09/2019.
//  Copyright © 2019 Synetech. All rights reserved.
//

#if canImport(UIKit) && canImport(ARKit) && canImport(Combine)
import ARKit
import Combine
import RealityKit
import Stevia

@available (iOS 13.0, *)
class ARScreenViewController: UIViewController {

    // MARK: - Views
    private lazy var sceneView = ARView(frame: .zero)

    private lazy var vStatusPanel = ARStatusView()

    // MARK: - Properties
    private let viewModel: ARScreenViewModel
    private let router: ARScreenRouter

    private var config = ARWorldTrackingConfiguration()
    private let options: ARSession.RunOptions = [.resetTracking,
                                                 .removeExistingAnchors]
    #if !targetEnvironment(simulator)
    private var session: ARSession {
        return sceneView.session
    }
    #endif
    private var scene: Scene {
        return sceneView.scene
    }

    private let edgeMargin: CGFloat = 24

    private let updateQueue = DispatchQueue(label: String(describing: ARScreenViewController.self))
    private var entityStream: [AnyCancellable] = []

    // MARK: - Init
    init(viewModel: ARScreenViewModel, router: ARScreenRouter) {
        self.viewModel = viewModel
        self.router = router
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.preloadEntities()
        setupViews()
        layoutViews()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        // Prevent the screen from being dimmed to avoid interuppting the AR experience.
        UIApplication.shared.isIdleTimerDisabled = true

        if AVCaptureDevice.authorizationStatus(for: .video) == .authorized {
            resetTrackingAndStartSession()
        } else {
            askForCameraPermission()
        }
    }

    private func askForCameraPermission() {
        AVCaptureDevice.requestAccess(for: .video) { [weak self] granted in
            DispatchQueue.main.async {
                if granted {
                    self?.resetTrackingAndStartSession()
                } else {
                    self?.router.onMissingCameraPermission()
                }
            }
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        #if !targetEnvironment(simulator)
        session.pause()
        #endif
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        viewModel.clearEntitiesCache()

        UIApplication.shared.isIdleTimerDisabled = false
    }

    // MARK: - Layout and setup
    private func setupViews() {
        #if !targetEnvironment(simulator)
        session.delegate = self
        #endif
    }

    private func layoutViews() {
        view.backgroundColor = .black
        view.sv(sceneView, vStatusPanel)

        sceneView.fillContainer()
        anchorToTop(vStatusPanel, offset: -edgeMargin)
        vStatusPanel.fillHorizontally(m: edgeMargin)
    }

    // MARK: - AR session management
    private func resetTrackingAndStartSession() {
        viewModel.removeAllEntitiesFromScene()

        if let referenceObjects = viewModel.getReferenceObjects() {
            config.detectionObjects = referenceObjects
            #if DEBUG
            print(referenceObjects)
            #endif
        } else {
            router.onFail()
        }

        #if !targetEnvironment(simulator)
        session.run(config, options: options)
        #endif
        vStatusPanel.showInfoMessage(.initialInstructions)
    }
}

@available (iOS 13.0, *)
extension ARScreenViewController: ARSessionDelegate {

    // MARK: - Session
    func session(_ session: ARSession, didAdd anchors: [ARAnchor]) {
        guard let objectAnchor = (anchors.first(where: { $0 is ARObjectAnchor })) as? ARObjectAnchor,
            let objectName = objectAnchor.name else { return }

        #if DEBUG
        vStatusPanel.showMessage("\(objectName) detected")
        print("Added anchor for \(objectName)")
        #endif
        prepareEntityForAddingToScene(name: objectName, on: objectAnchor)
    }

    private func prepareEntityForAddingToScene(name: String, on anchor: ARAnchor) {
        updateQueue.async { [weak self] in
            guard let self = self else { return }
            var cancellable: AnyCancellable?
            cancellable = self.viewModel.getEntity(for: name)?
                .sink(receiveCompletion: { _ in },
                      receiveValue: { [weak self] entity in
                        guard let entity = entity else { return }
                        #if !targetEnvironment(simulator)
                        entity.anchoring = AnchoringComponent(.anchor(identifier: anchor.identifier))
                        #endif
                        self?.addToScene(entity)
                        self?.entityStream.removeAll(where: { $0 === cancellable })
                })
            cancellable?.store(in: &self.entityStream)
        }
    }

    private func addToScene(_ entity: HasAnchoring) {
        DispatchQueue.main.async { [weak self] in

            self?.scene.addAnchor(entity)
            #if DEBUG
            debugPrint("\(entity.anchoring) added to scene")
            #endif

            self?.viewModel.disableAllEntities()
            self?.viewModel.setEntityIsInScene(entity)
        }
    }

    func session(_ session: ARSession, didUpdate anchors: [ARAnchor]) {
        guard let objectAnchor = (anchors.first(where: { $0 is ARObjectAnchor })) as? ARObjectAnchor,
            let objectName = objectAnchor.name else { return }

        if !viewModel.isEntityEnabled(name: objectName) {
            viewModel.disableAllEntities()
            viewModel.enableEntity(name: objectName)
        }
    }

    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        switch camera.trackingState {
        case .limited(.excessiveMotion), .limited(.insufficientFeatures):
            vStatusPanel.showTrackingStateMessage(camera.trackingState)
        default:
            break
        }
    }

    func session(_ session: ARSession, didFailWithError error: Error) {
        guard error is ARError else { return }

        router.onFail()
    }

    func sessionWasInterrupted(_ session: ARSession) {
        // Hide content before going into the background.
        viewModel.removeAllEntitiesFromScene()
    }

    func sessionInterruptionEnded(_ session: ARSession) {
        resetTrackingAndStartSession()
    }
}

#endif
