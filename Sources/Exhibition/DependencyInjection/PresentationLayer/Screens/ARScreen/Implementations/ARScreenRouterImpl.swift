//
//  ARScreenRouterImpl.swift
//  AugmentedExhibition
//
//  Created by Lukáš Růžička on 26/09/2019.
//  Copyright © 2019 Synetech. All rights reserved.
//

#if canImport(ARKit)
import Foundation

@available (iOS 13.0, *)
class ARScreenRouterImpl {

    // MARK: - Properties
    private let libRouter: LibRouter

    // MARK: - Init
    init(libRouter: LibRouter) {
        self.libRouter = libRouter
    }
}

@available (iOS 13.0, *)
extension ARScreenRouterImpl: ARScreenRouter {

    func onBackTapped() {
        libRouter.onCancel()
    }

    func onMissingCameraPermission() {
        libRouter.emmitEvent(.noCameraAccess)
    }

    func onFail() {
        libRouter.emmitEvent(.fail)
    }
}
#endif
