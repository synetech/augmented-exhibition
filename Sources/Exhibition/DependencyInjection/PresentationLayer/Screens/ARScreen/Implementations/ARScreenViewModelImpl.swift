//
//  ARScreenViewModelImpl.swift
//  AugmentedExhibition
//
//  Created by Lukáš Růžička on 26/09/2019.
//  Copyright © 2019 Synetech. All rights reserved.
//

#if canImport(ARKit) && canImport(Combine)
import ARKit
import Combine
import RealityKit

@available (iOS 13.0, *)
class ARScreenViewModelImpl {

    // MARK: - Properties
    private let referenceObjects: Set<ARReferenceObject>
    private let detectionObjects: [DetectionObject]

    private var entitiesCache: [EntityCacheModel] = []
    private var entitiesInScene: [EntityCacheModel] {
        return entitiesCache.filter { $0.inScene }
    }

    private var cancellables = Set<AnyCancellable>()

    // MARK: - Init
    init(referenceObjects: Set<ARReferenceObject>,
         detectionObjects: [DetectionObject]) {
        self.referenceObjects = referenceObjects
        self.detectionObjects = detectionObjects
    }
}

@available (iOS 13.0, *)
extension ARScreenViewModelImpl: ARScreenViewModel {

    // MARK: - Reference objects
    func getReferenceObjects() -> Set<ARReferenceObject>? {
        return referenceObjects
    }

    // MARK: - Entities
    func preloadEntities() {
        for object in detectionObjects {
            let entityModel = EntityCacheModel(identifier: object.identifier)
            entitiesCache.append(entityModel)
            object.entity
                .sink(receiveCompletion: { _ in },
                      receiveValue: { entity in
                        entity.isEnabled = false
                        entityModel.entity.send(entity)
                        #if DEBUG
                        print("\(object.identifier) loaded")
                        #endif
                })
                .store(in: &cancellables)
        }
    }

    func getEntity(for name: String) -> CurrentValueSubject<HasAnchoring?, Error>? {
        guard let entityModel = entitiesCache.first(where: { $0.identifier == name }) else { return nil }
        return entityModel.entity
    }

    func disableAllEntities() {
        entitiesInScene.forEach { $0.entity.value?.isEnabled = false }
    }

    func enableEntity(name: String) {
        entitiesInScene.first(where: { $0.identifier == name })?.entity.value?.isEnabled = true
    }

    func isEntityEnabled(name: String) -> Bool {
        return entitiesInScene.first(where: { $0.identifier == name })?.entity.value?.isEnabled ?? false
    }

    func setEntityIsInScene(_ entity: HasAnchoring) {
        guard let entityModel = entitiesCache
            .filter({ $0.entity.value != nil })
            .first(where: { $0.entity.value! == entity }) else { return }
        entityModel.inScene = true
        entityModel.entity.value?.isEnabled = true
    }

    func getAllEntitiesInScene() -> [HasAnchoring] {
        return entitiesInScene
            .filter { $0.entity.value != nil }
            .map { $0.entity.value! }
    }

    func removeAllEntitiesFromScene() {
        entitiesInScene.forEach { entityModel in
            entityModel.entity.value?.removeFromParent()
            entityModel.inScene = false
        }
    }

    func clearEntitiesCache() {
        removeAllEntitiesFromScene()
        entitiesCache = []
    }
}
#endif
