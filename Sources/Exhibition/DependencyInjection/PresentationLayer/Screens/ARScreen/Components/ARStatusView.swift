//
//  ARStatusView.swift
//  AugmentedExhibition
//
//  Created by Lukáš Růžička on 26/09/2019.
//  Copyright © 2019 Synetech. All rights reserved.
//

#if canImport(UIKit) && canImport(ARKit)
import ARKit
import Stevia

@available (iOS 13.0, *)
class ARStatusView: RecommendationView {

    // MARK: - Interactions
    func showTrackingStateMessage(_ trackingState: ARCamera.TrackingState) {
        guard let recommendation = trackingState.recommendation,
            let showTime = trackingState.recommendationShowTime else {
                return
        }

        showMessage(recommendation, showTime: showTime)
    }

    func showInfoMessage(_ infoMessage: ARInfoMessages) {
        showMessage(infoMessage.message, showTime: infoMessage.showTime)
    }
}

enum ARInfoMessages: LibTranslations {

    case initialInstructions

    var message: String {
        switch self {
        case .initialInstructions:
            return getString(for: .initialInstructions)
        }
    }

    var showTime: TimeInterval {
        switch self {
        case .initialInstructions:
            return 6
        }
    }
}

#endif
