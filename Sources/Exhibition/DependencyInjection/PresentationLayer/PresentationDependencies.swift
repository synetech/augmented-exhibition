//
//  PresentationDependencies.swift
//  AugmentedExhibition
//
//  Created by Lukáš Růžička on 20/09/2019.
//  Copyright © 2019 Synetech. All rights reserved.
//

#if canImport(ARKit)
import Swinject

// swiftlint:disable force_unwrapping
@available (iOS 13.0, *)
class PresentationDependencies: DI {

    static func build(parent: Container?, depth: DIDepth, config: AEConfiguration, router: LibRouter?) -> Container {
        let container = Container(parent: parent, defaultObjectScope: .graph)

        registerViewModels(container, config: config)
        registerRouters(container, router: router)
        registerInteractionHandlers(container)
        registerViewControllers(container)

        return container
    }

    private static func registerViewModels(_ container: Container, config: AEConfiguration) {
        container.register(ARScreenViewModel.self) { _ in
            ARScreenViewModelImpl(referenceObjects: config.referenceObjects,
                                  detectionObjects: config.detectionObjects)
        }
    }

    private static func registerRouters(_ container: Container, router: LibRouter?) {
        guard let router = router else {
            fatalError("Pod router must be specified")
        }
        container.register(LibRouter.self) { _ in
            router
        }

        container.register(ARScreenRouter.self) { r in
            let libRouter = r.resolve(LibRouter.self)!
            return ARScreenRouterImpl(libRouter: libRouter)
        }
    }

    private static func registerInteractionHandlers(_ container: Container) {
    }

    private static func registerViewControllers(_ container: Container) {
        container.register(ARScreenViewController.self) { r in
            let viewModel = r.resolve(ARScreenViewModel.self)!
            let router = r.resolve(ARScreenRouter.self)!
            return ARScreenViewController(viewModel: viewModel, router: router)
        }
    }
}
#endif
