//
//  DependencyInjectionProtocol.swift
//  AugmentedExhibition
//
//  Created by Lukáš Růžička on 20/09/2019.
//  Copyright © 2019 Synetech. All rights reserved.
//

#if canImport(ARKit)
import Foundation
import Swinject

@available (iOS 13.0, *)
protocol DI {

    static func build(parent: Container?, depth: DIDepth, config: AEConfiguration, router: LibRouter?) -> Container
}

enum DIDepth: Int {
    case dataSource
    case repository
    case domain
    case presentation
}
#endif
