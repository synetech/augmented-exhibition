//
//  DataSourceDependencies.swift
//  AugmentedExhibition
//
//  Created by Lukáš Růžička on 20/09/2019.
//  Copyright © 2019 Synetech. All rights reserved.
//

import Swinject

#if canImport(ARKit)
@available (iOS 13.0, *)
class DataSourceDependencies: DI {

    static func build(parent: Container?, depth: DIDepth, config: AEConfiguration, router: LibRouter?) -> Container {
        let container = Container(parent: parent, defaultObjectScope: .container)

        return depth == .dataSource ? container
            : RepositoryDependencies.build(parent: container, depth: depth, config: config, router: router)
    }
}
#endif
