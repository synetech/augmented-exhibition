//
//  RepositoryDependencies.swift
//  AugmentedExhibition
//
//  Created by Lukáš Růžička on 20/09/2019.
//  Copyright © 2019 Synetech. All rights reserved.
//

#if canImport(ARKit)
import Swinject

// swiftlint:disable force_unwrapping
@available (iOS 13.0, *)
class RepositoryDependencies: DI {

    static func build(parent: Container?, depth: DIDepth, config: AEConfiguration, router: LibRouter?) -> Container {
        let container = Container(parent: parent, defaultObjectScope: .container)

        registerRepositories(container)
        resolveStaticRepositories(container, config: config)

        return depth == .repository ? container
            : DomainDependencies.build(parent: container, depth: depth, config: config, router: router)
    }

    private static func registerRepositories(_ container: Container) {
    }

    private static func resolveStaticRepositories(_ container: Container, config: AEConfiguration) {
        _ = LocalizationRepository(translations: config.translations)
        _ = FontRepository(fontNames: config.fontNames)
    }
}
#endif
