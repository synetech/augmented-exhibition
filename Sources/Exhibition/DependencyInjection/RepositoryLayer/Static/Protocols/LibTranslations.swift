//
//  LibTranslations.swift
//  
//
//  Created by Lukáš Růžička on 06/11/2019.
//

import Foundation

protocol LibTranslations {

    func getString(for key: LocalizationKey) -> String
}

extension LibTranslations {

    func getString(for key: LocalizationKey) -> String {
        return LocalizationRepository.sharedInstance.getString(for: key)
    }
}

enum LocalizationKey {

    case initialInstructions
    case recommendationForExcessiveMotion
    case recommendationForInsufficientFeatures
}
