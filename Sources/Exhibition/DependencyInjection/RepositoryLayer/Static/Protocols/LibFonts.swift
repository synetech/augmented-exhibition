//
//  LibFonts.swift
//  
//
//  Created by Lukáš Růžička on 05/11/2019.
//

#if canImport(UIKit)
import UIKit

protocol LibFonts {

    func getFont(for key: FontKey, ofSize size: CGFloat) -> UIFont?
}

extension LibFonts {

    func getFont(for key: FontKey, ofSize size: CGFloat) -> UIFont? {
        return FontRepository.sharedInstance.getFont(for: key, ofSize: size)
    }
}

enum FontKey: CaseIterable {

    case regular
    case bold
}
#endif
