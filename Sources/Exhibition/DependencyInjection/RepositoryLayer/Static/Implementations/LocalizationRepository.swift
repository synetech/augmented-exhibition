//
//  LocalizationRepository.swift
//  
//
//  Created by Lukáš Růžička on 06/11/2019.
//

import Foundation

class LocalizationRepository {

    // MARK: - Static
    // swiftlint:disable:next implicitly_unwrapped_optional
    static var sharedInstance: LibTranslations!

    // MARK: - Properties
    private let translations: AETranslations

    // MARK: - Init
    init(translations: AETranslations?) {
        self.translations = translations ?? Self.getDefaultTranslations()
        LocalizationRepository.sharedInstance = self
    }

    // MARK: - Default
    private static func getDefaultTranslations() -> AETranslations {
        return AETranslations(initialInstructions: "Point your device on the objects",
                              recommendationForExcessiveMotion: "Excessive motion, try to slow down a little bit",
                              recommendationForInsufficientFeatures: "Insufficient features, maybe it's too dark around")
    }
}

extension LocalizationRepository: LibTranslations {

    func getString(for key: LocalizationKey) -> String {
        switch key {
        case .initialInstructions:
            return translations.initialInstructions
        case .recommendationForExcessiveMotion:
            return translations.recommendationForExcessiveMotion
        case .recommendationForInsufficientFeatures:
            return translations.recommendationForInsufficientFeatures
        }
    }
}
