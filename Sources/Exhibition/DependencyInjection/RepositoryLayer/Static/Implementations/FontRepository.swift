//
//  FontRepository.swift
//  
//
//  Created by Lukáš Růžička on 05/11/2019.
//

#if canImport(UIKit)
import UIKit

class FontRepository {

    // MARK: - Static
    // swiftlint:disable:next implicitly_unwrapped_optional
    static var sharedInstance: LibFonts!

    // MARK: - Properties
    private let fontNames: FontNames?

    // MARK: - Init
    init(fontNames: FontNames?) {
        self.fontNames = fontNames
        FontRepository.sharedInstance = self
    }
}

extension FontRepository: LibFonts {

    func getFont(for key: FontKey, ofSize size: CGFloat) -> UIFont? {
        guard let fontNames = fontNames else {
            return nil
        }
        switch key {
        case .regular:
            return UIFont(name: fontNames.regular, size: size)
        case .bold:
            return UIFont(name: fontNames.bold ?? fontNames.regular, size: size)
        }
    }
}
#endif
