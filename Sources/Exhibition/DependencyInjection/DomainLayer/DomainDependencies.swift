//
//  DomainDependencies.swift
//  AugmentedExhibition
//
//  Created by Lukáš Růžička on 20/09/2019.
//  Copyright © 2019 Synetech. All rights reserved.
//

#if canImport(ARKit)
import Swinject

// swiftlint:disable force_unwrapping
@available (iOS 13.0, *)
class DomainDependencies: DI {

    static func build(parent: Container?, depth: DIDepth, config: AEConfiguration, router: LibRouter?) -> Container {
        let container = Container(parent: parent, defaultObjectScope: .container)

        registerUseCases(container)

        return depth == .domain ? container
            : PresentationDependencies.build(parent: container, depth: depth, config: config, router: router)
    }

    private static func registerUseCases(_ container: Container) {
    }
}
#endif
