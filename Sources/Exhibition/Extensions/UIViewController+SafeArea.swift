//
//  UIViewController+SafeArea.swift
//  AugmentedExhibition
//
//  Created by Lukáš Růžička on 05/11/2019.
//  Copyright © 2019 Synetech. All rights reserved.
//

#if canImport(UIKit)
import UIKit

/// Safe area extensions which should be called on UIViewController
extension UIViewController {

    /// Aligns the view to the top of safe area (not the window!)
    /// Very useful for X iPhones, where is critical to not place views outside the top of safe area (notch)
    ///
    /// - Parameters:
    ///   - viewToAnchor: view to be aligned to the safe area edge
    ///   - offset: distance from the safe area edge (optional, default is 0)
    func anchorToTop(_ viewToAnchor: UIView, offset: CGFloat = 0) {
        if #available(iOS 11, *) {
            let guide = self.view.safeAreaLayoutGuide
            NSLayoutConstraint.activate([
                guide.topAnchor.constraint(equalTo: viewToAnchor.topAnchor, constant: offset)
                ])
        } else {
            NSLayoutConstraint.activate([
                self.topLayoutGuide.bottomAnchor.constraint(equalTo: viewToAnchor.topAnchor, constant: offset)
                ])
        }
    }

    /// Aligns the view to the bottom of safe area (not the window!)
    /// Very useful for X iPhones, where is critical to not place views outside the bottom of safe area (navigation line)
    ///
    /// - Parameters:
    ///   - viewToAnchor: view to be aligned to the safe area edge
    ///   - offset: distance from the safe area edge (optional, default is 0)
    func anchorToBottom(_ viewToAnchor: UIView, offset: CGFloat = 0) {
        if #available(iOS 11, *) {
            let guide = self.view.safeAreaLayoutGuide
            NSLayoutConstraint.activate([
                guide.bottomAnchor.constraint(equalTo: viewToAnchor.bottomAnchor, constant: offset)
                ])
        } else {
            NSLayoutConstraint.activate([
                self.bottomLayoutGuide.topAnchor.constraint(equalTo: viewToAnchor.bottomAnchor, constant: offset)
                ])
        }
    }
}
#endif
