//
//  ARTrackingState+Recommendations.swift
//  AugmentedExhibition
//
//  Created by Lukáš Růžička on 26/09/2019.
//  Copyright © 2019 Synetech. All rights reserved.
//

#if canImport(ARKit)
import ARKit

@available (iOS 13.0, *)
extension ARCamera.TrackingState: LibTranslations {

    var recommendation: String? {
        switch self {
        case .limited(.excessiveMotion):
            return getString(for: .recommendationForExcessiveMotion)
        case .limited(.insufficientFeatures):
            return getString(for: .recommendationForInsufficientFeatures)
        default:
            return nil
        }
    }

    var recommendationShowTime: TimeInterval? {
        switch self {
        case .limited(.excessiveMotion), .limited(.insufficientFeatures):
            return 5
        default:
            return nil
        }
    }

    func isNormal() -> Bool {
        switch self {
        case .normal:
            return true
        default:
            return false
        }
    }
}
#endif
