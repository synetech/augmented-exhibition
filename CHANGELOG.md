# Changelog

## 0.2.0 Release notes (14-11-2019)

### Breaking Changes
- Loading scenes asynchronously.

### Enhancements
- Showing only one scene at once.


## 0.0.3 Release notes (08-11-2019)

### Enhancements
- Content for camera exhibit prepared.
- Preloading content for AR.


## 0.0.2 Release notes (06-11-2019)

### Enhancements
- Added font customization.
- Checking camera permission.
- Emmitting events to handle in implementing app.
- Added translations customization.


## 0.0.1 Release notes (05-11-2019)

### Breaking Changes
- Project initialized.
- Project prepared to be used as Swift package or Cocoapod.
- Basic AR screen prepared to use.


## x.x.x Release notes (dd-mm-yyyy)

### Breaking Changes
### Deprecated
### Enhancements
### Bugfixes
